package com.upload.uploadcropbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UploadCropBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(UploadCropBackendApplication.class, args);
	}

}
