How to Run Individually
-- Build Server image

docker build -t upload.

-- Run Service image

docker run --name demo -p 8080:8080 -t demo

-- Inspect Docker container to find ip address to configure in UI

docker inspect -f "{{ .NetworkSettings.IPAddress }}"

-- Build Client image

docker build -t upload-crop-frontend .

-- Run Client image

docker run --name upload-crop-frontend -p 4200:80 -t upload-crop-frontend:latest

How to Run with each other
docker-compose up