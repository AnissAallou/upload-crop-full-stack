import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadedImage } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrl: './image-upload.component.scss'
})
export class ImageUploadComponent {


  imageChangedEvent: any = '';
  croppedImage: any = '';

  constructor(
    private sanitizer: DomSanitizer
  ) {
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: any): void {
    this.croppedImage = this.sanitizer.bypassSecurityTrustUrl(event.objectUrl);
    // event.blob can be used to upload the cropped image
  }

  
  imageLoaded($event: LoadedImage) {
   // show cropper
  }

  cropperReady() {
   // cropper ready
  }
    

  loadImageFailed() {
    // show message
  }

  downloadCroppedImage(): void {
    const downloadLink = document.createElement('a');
    downloadLink.href = this.croppedImage;
    downloadLink.download = 'cropped_image.png';
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

}
